<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$names = [
			'Prabhat Sigmundr',
			'Loukianos Urbano',
			'Morgan Walton',
			'Harsh Hrólfr',
			'Fran Cináed',
			'Yahweh Yehiel',
			'Shelley Jagannatha',
			'Abidemi Jooseppi'
		];

		for($i=0;$i<8;$i++)
		{
			User::create(
				[
					'name' => $names[$i],
					'email' => 'mail'.rand().'@mail.com',
					'password' => '123456',
					'description' => 'What does this team member to? Keep it short! This is also a great spot for social links!',
					'role' => 'developer',
				]
			);
		}
	}
}
