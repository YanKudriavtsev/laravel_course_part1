<?php

use App\StaticPage;
use Illuminate\Database\Seeder;

class StaticPagesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		StaticPage::create(
			[
				'content'=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint, explicabo dolores ipsam aliquam inventore corrupti eveniet quisquam quod totam laudantium repudiandae obcaecati ea consectetur debitis velit facere nisi expedita vel?',
				'page_type' => 'about',
			]
		);
	}
}
