<?php

use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i=0; $i < 5; $i++){
            Category::create(
                [
                    'author_id' => $faker->numberBetween(10,20),
                    'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                    'content' => $faker->text($maxNbChars = 2000),
                    'image' => 'http://path_to_image'
                ]
            );
        }
    }
}
