<?php

use App\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create();

		for($i=0; $i < 5; $i++){
			Article::create(
				[
					'author_id' => $faker->numberBetween(10,20),
					'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
					'content' => $faker->text($maxNbChars = 2000),
					'image' => 'http://path_to_image'
				]
			);
		}
	}
}
