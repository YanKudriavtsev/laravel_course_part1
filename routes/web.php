<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'IndexController@show', 'as' => 'home']);

Route::get(
    '/article/{id}',
    [
        'as' => 'article',
        'uses' => 'ArticleController@getArticle'
    ]
);
    
Route::get(
    '/articles',
    [
        'as' => 'articles',
        'uses' => 'ArticleController@getArticles'
    ]
);

Route::get('/about', ['uses' => 'AboutController@show', 'as' => 'about']);

Route::match(
    ['get', 'post'],
    '/contact-us',
    [
        'uses' => 'ContactUsController@index',
        'as' => 'contact_us'
    ]
);


Auth::routes();
