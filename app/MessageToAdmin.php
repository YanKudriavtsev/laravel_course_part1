<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageToAdmin extends Model
{
    protected $table = 'messages_to_admins';

    protected $fillable = [
        'sender_name',
        'sender_email',
        'message'
    ];
}
