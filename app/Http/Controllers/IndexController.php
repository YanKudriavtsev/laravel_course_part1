<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
	protected $request;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function show()
	{
		$data = [
			'pageHeader' => 'Home',
		];

		return view('index', $data);
	}
}
