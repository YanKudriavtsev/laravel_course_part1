<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
	public function getArticles()
	{
		$data = [
			'articles' => Article::all(),
			'pageHeader' => 'Articles'
		];

		return view('article/articles', $data);
	}

	public function getArticle($articleId)
	{
		$data = [
			'pageHeader' => 'Article'
		];

		return view('article/article', $data);
	}
}
