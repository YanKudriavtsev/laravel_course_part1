<?php

namespace App\Http\Controllers;

use App\StaticPage;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AboutController extends Controller
{
	public function show()
	{
		$data = [
			'pageHeader' => "About Us It's Nice to Meet You!",
			'users' => User::where('status', 1)->where('role', 'developer')->get(),
			'page' => StaticPage::where('page_type', 'about')->first(),
		];
		return view('about', $data);
	}
}
