<?php

namespace App\Http\Controllers;

use App\MessageToAdmin;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    protected $request;

    private $rules = [
        'name' => 'required|string|min:2|max:50',
        'email' => 'required|email|min:4|max:60',
        'text' => 'required|string|min:10|max:1000'
    ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        if($this->request->method() == 'GET') {
            $data = [
                'pageHeader' => 'Contact us'
            ];

            return view('contactUs', $data);
        } else {
            $this->validate($this->request, $this->rules);

            MessageToAdmin::create([
                'sender_name' => $this->request->name,
                'sender_email' => $this->request->email,
                'message' => $this->request->text
            ]);

            return redirect()->route('home');
        }
    }
}
