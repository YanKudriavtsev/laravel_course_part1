@extends('layouts.layout')

@section('content')
    @if($errors->any())
        @foreach($errors->all() as $error)
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <div class="form-area">
        <form role="form" action="/contact-us" method="POST">
            {{ csrf_field() }}
            <br style="clear:both">
            <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" placeholder="Full name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <textarea class="form-control" name="text" id="message" placeholder="Message. Max: 1000 characters" maxlength="1000" rows="7"></textarea>
            </div>

            <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right form-control">Submit Form</button>
        </form>
        <hr>
    </div>
@endsection