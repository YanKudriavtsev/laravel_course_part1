@extends('layouts.layout')

@section('content')
	<p>{{ $page->content }}</p>

	<!-- Team Members Row -->
	<div class="row">
		<div class="col-lg-12">
			<h2 class="my-4">Our Team</h2>
		</div>

		@foreach($users as $user)
			<div class="col-lg-4 col-sm-6 text-center mb-4">
				<img class="rounded-circle img-fluid d-block mx-auto" src="http://placehold.it/200x200" alt="">
				<h4>{{ $user->name }}
					<br><small>Job Title</small>
				</h4>
				<p>{{ $user->description }}</p>
			</div>
		@endforeach
	</div>

@endsection