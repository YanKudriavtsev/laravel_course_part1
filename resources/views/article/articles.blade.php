@extends('layouts/layout')

@section('content')
	@foreach($articles as $article)
		<!-- Blog Post -->
		<div class="card mb-4">
			<img class="card-img-top" src="http://placehold.it/750x300" alt="Card image cap">
			<div class="card-body">
				<h2 class="card-title">{{ $article->title }}</h2>
				<p class="card-text">{!! $article->content !!}</p>
				<a href="{{ route('article', $article->id) }}" class="btn btn-primary">Read More &rarr;</a>
			</div>
			<div class="card-footer text-muted">
				{{ $article->created_at }}
				{{-- Posted on January 1, 2017 by --}}
				<a href="#">Start Bootstrap</a>
			</div>
		</div>
	@endforeach
@endsection